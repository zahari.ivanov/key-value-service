# Key-Value Service

Simple Key-Value service. It is created for teaching purposed in order to be integrated with various golang tools.

## Architecture
<img src="kv-service-class-diagram.jpg" alt="KeyValue class diagram"/>

Service provides business logic for KeyValue pairs. It wraps abstract DBManager for data access operations. 
DBManager is abstracted which makes separation of core logic from low level details.

Cockroach DBManager - provides implementation of DBManager in order to wire up KeyValue service with Cockroach db.

Prometheus - integrated in order to collect metrics for latency of requests for KeyValuePair creation.

View - registers endpoints of KeyValueService


## Tools

    1. Ginkogo & Gomega
    2. Logrus
    3. Linter
    4. Metrics & Tracing - https://opencensus.io/tracing/
    
   
   
   
## Run Server

From root project dir <br>
`go build .` <br>
`./kv-store`

## API 

Simple request for KeyValuePair entity creation

Method: POST <br>

`http://localhost:8081/kv-store`

Body:

```json 
{
 	"key" : "mykey",
 	"value" : "myvalue",
 	"bucket" : "mybucket",
 	"user" : "jdoe3"
 }


   