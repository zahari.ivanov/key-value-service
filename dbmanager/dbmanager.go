package dbmanager

import (
	"database/sql"
	"key-value-service/model"
)

// Implementation of dbManager interface defined in "service" package
type CockroachDBManager struct {
	DBsession *sql.DB
}

// SaveKeyValuePair - stores KeyValuePair entity in db
func (c CockroachDBManager) SaveKeyValuePair(kv *model.KeyValuePair) (*model.KeyValuePair, error) {

	if _, err := c.DBsession.Exec(
		`INSERT INTO kv_store (key, value, bucket, username)
		 VALUES ($1, $2, $3, $4);`, kv.Key, kv.Value, kv.Bucket, kv.User); err != nil {
		return nil, err
	}

	return kv, nil
}

// RetrieveKeyValuePair - retrieves single key-value pair
func (c CockroachDBManager) RetrieveKeyValuePair(key, bucket, user string) (*model.KeyValuePair, error) {
	row := c.DBsession.QueryRow("select key, value, bucket, user FROM kv_store where key=%s and bucket=%s and user=%s limit=1;", key, bucket, user)
	var (
		keyResponse    string
		valueResponse  string
		bucketResponse string
		userResponse   string
	)

	err := row.Scan(&keyResponse, &valueResponse, &bucketResponse, &userResponse)
	if err != nil {
		return nil, err
	}

	result := &model.KeyValuePair{
		Key:    keyResponse,
		Value:  valueResponse,
		Bucket: bucketResponse,
		User:   userResponse,
	}

	return result, nil
}

// RetrieveAllKeyValuePairsForUser - Retrieves all KeyValuePair entities.
// [Disclaimer] Pagination (limit & offset) is not supported.
func (c CockroachDBManager) RetrieveAllKeyValuePairsForUser(user string) ([]model.KeyValuePair, error) {
	rows, err := c.DBsession.Query("select key, value, bucket, user FROM kv_store where user=$1;", user)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	allKVPairs := []model.KeyValuePair{}

	for rows.Next() {
		var key string
		var value string
		var bucket string
		var user string
		if err := rows.Scan(&key, &value, &bucket, &user); err != nil {
			return nil, err
		}

		result := model.KeyValuePair{
			Key:    key,
			Value:  value,
			Bucket: bucket,
			User:   user,
		}

		allKVPairs = append(allKVPairs, result)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}
	return allKVPairs, nil
}
