package main

import (
	"database/sql"
	"key-value-service/prometheus"
	"key-value-service/dbmanager"
	"key-value-service/service"
	"key-value-service/view"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

func main() {
	// Open connection to db
	db, err := initDbConnection()
	if err != nil {
		log.Fatalf("Unable to establish db connection: %v", err)
	}

	// Initialize db manager with above connection
	dbManager := dbmanager.CockroachDBManager{
		DBsession: db,
	}
	// Initialize Key-Value Store Service
	kvService := service.NewKVService(dbManager)

	// Initialize latency measure metrics - TODO use decorator
	latencyMeasure := prometheus.NewMetricsLatency()

	// Create router
	router := mux.NewRouter().StrictSlash(true)

	// Register endpoints for kv-service
	view.NewConfigurationEndpoint(kvService, latencyMeasure, router)

	prometheus.StartPrometheusServer()

	log.Fatal(http.ListenAndServe(":8081", router))
}

// initDbConnection - initialize connection to db with hardcoded connection string
func initDbConnection() (*sql.DB, error) {
	// Connect to the "kv_store" database.
	db, err := sql.Open("postgres", "postgresql://root@localhost:26257/kv_store?sslmode=disable")
	if err != nil {
		//log.Fatal("error connecting to the database: ", err)
		return nil, err
	}

	return db, nil
}
