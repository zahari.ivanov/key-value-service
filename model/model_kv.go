package model

// KeyValuePair - represents model of Key-Value entity
type KeyValuePair struct {
	Key    string `json:"key"`
	Value  string `json:"value"`
	Bucket string `json:"bucket"`
	User   string `json:"user"`
}
