package prometheus

import (
	"context"
	"go.opencensus.io/stats"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/tag"
	"log"
	"time"
)

type MetricsLatency struct {
	MLatencyMs *stats.Float64Measure
}

// Implementation of Metrics interface declared in "view" package
func (l *MetricsLatency) MeasureLatency(start time.Time) {
	stats.Record(context.Background(), l.MLatencyMs.M(sinceInMilliseconds(start)))
}

// calculates duration of interval since provided "startTime"
func sinceInMilliseconds(startTime time.Time) float64 {
	return float64(time.Since(startTime).Nanoseconds()) / 1e6
}

func NewMetricsLatency() *MetricsLatency {
	keyMethod, _ := tag.NewKey("method")
	mLatencyMS := stats.Float64("kvstore/latency", "The latency in milliseconds per request", "ms")
	latencyMeasure := MetricsLatency{
		MLatencyMs: mLatencyMS,
	}

	// Register the views
	if err := view.Register(&view.View{Name: "kvstore/latency-view",
		Measure:     mLatencyMS,
		Description: "The distribution of the latencies",

		// Latency in buckets:
		// [>=0ms, >=25ms, >=50ms, >=75ms, >=100ms, >=200ms, >=400ms, >=600ms, >=800ms, >=1s, >=2s, >=4s, >=6s]
		Aggregation: view.Distribution(0, 25, 50, 75, 100, 200, 400, 600, 800, 1000, 2000, 4000, 6000),
		TagKeys:     []tag.Key{keyMethod}}); err != nil {
		log.Fatalf("Failed to register the views: %v", err)
	}

	return &latencyMeasure
}
