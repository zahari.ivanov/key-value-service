package prometheus

import (
	"contrib.go.opencensus.io/exporter/prometheus"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func StartPrometheusServer() {
	// Create the Metrics exporter.
	pe, err := prometheus.NewExporter(prometheus.Options{
		Namespace: "kvstore",
	})
	if err != nil {
		log.Fatalf("Failed to create the Metrics stats exporter: %v", err)
	}
	go func() {
		mux := http.NewServeMux()
		mux.Handle("/metrics", pe)
		if err := http.ListenAndServe(":8888", mux); err != nil {
			log.Fatalf("Failed to run Metrics scrape endpoint: %v", err)
		}
	}()
}
