package service

import (
	"errors"
	"key-value-service/model"
)

type DBManager interface {
	SaveKeyValuePair(kv *model.KeyValuePair) (*model.KeyValuePair, error)

	RetrieveKeyValuePair(key, bucket, user string) (*model.KeyValuePair, error)

	RetrieveAllKeyValuePairsForUser(user string) ([]model.KeyValuePair, error)
}

type KeyValueService struct {
	dbManager DBManager
}

func (k KeyValueService) CreateKeyValuePair(kv *model.KeyValuePair) (*model.KeyValuePair, error) {

	if kv.Key == "" {
		return nil, errors.New("'key' is not provided")
	}
	if kv.Value == "" {
		return nil, errors.New("'value' is not provided")
	}
	if kv.Bucket == "" {
		return nil, errors.New("'bucket' is not provided")
	}
	if kv.User == "" {
		return nil, errors.New("'user' is not provided")
	}

	createdKV, err := k.dbManager.SaveKeyValuePair(kv)
	if err != nil {
		return nil, err
	}

	return createdKV, nil
}

func (k KeyValueService) GetKeyValuePair(key, bucket, user string) (*model.KeyValuePair, error) {
	//get key-value pair
	kv, err := k.dbManager.RetrieveKeyValuePair(key, bucket, user)
	if err != nil {
		return nil, err
	}
	return kv, nil
}

func (k KeyValueService) GetAllKeyValuePairsForUser(user string) ([]model.KeyValuePair, error) {
	//get all key-value pairs
	kvList, err := k.dbManager.RetrieveAllKeyValuePairsForUser(user)
	if err != nil {
		return nil, err
	}
	return kvList, nil
}

func NewKVService(dbManager DBManager) *KeyValueService {
	return &KeyValueService{dbManager: dbManager}
}
