package service_test

import (
	"database/sql"
	"key-value-service/dbmanager"
	"key-value-service/model"
	"key-value-service/service"
	"regexp"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var _ = Describe("Service", func() {

	var mock sqlmock.Sqlmock
	var dbManager service.DBManager
	var kvService service.KeyValueService

	BeforeEach(func() {

		var db *sql.DB
		var err error

		db, mock, err = sqlmock.New() // mock sql.DB
		Expect(err).ShouldNot(HaveOccurred())

		dbManager = dbmanager.CockroachDBManager{DBsession: db}

		kvService = *service.NewKVService(dbManager)
	})

	AfterEach(func() {
		err := mock.ExpectationsWereMet() // make sure all expectations were met
		Expect(err).ShouldNot(HaveOccurred())
	})

	Describe("List all key-value pairs", func() {
		Context("empty", func() {
			It("Empty list", func() {
				const sqlSelectAll = `select key, value, bucket, user FROM kv_store where user=$1;`
				mock.ExpectQuery(regexp.QuoteMeta(sqlSelectAll)).
					WillReturnRows(sqlmock.NewRows(nil))

				kv, err := kvService.GetAllKeyValuePairsForUser("jdoe")
				Expect(err).ShouldNot(HaveOccurred())
				Expect(kv).Should(BeEmpty())
			})

		})
	})

	Describe("Create Key-Value pair", func() {
		Context("Create single key-value pair", func() {
			It("Create one key-value pair", func() {

				keyValuePair := &model.KeyValuePair{
					Key:    "key",
					Value:  "value",
					Bucket: "bucket",
					User:   "username",
				}

				const createKV = `INSERT INTO kv_store (key, value, bucket, username) VALUES ($1, $2, $3, $4);`

				mock.ExpectExec(regexp.QuoteMeta(createKV)).
					WithArgs(keyValuePair.Key, keyValuePair.Value,
						keyValuePair.Bucket, keyValuePair.User).
					WillReturnResult(sqlmock.NewResult(1, 1))

				createdKVPair, err := kvService.CreateKeyValuePair(keyValuePair)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(createdKVPair.Key).Should(Equal(keyValuePair.Key))
				Expect(createdKVPair.Value).Should(Equal(keyValuePair.Value))
				Expect(createdKVPair.Bucket).Should(Equal(keyValuePair.Bucket))
				Expect(createdKVPair.User).Should(Equal(keyValuePair.User))
			})
		})
	})

})
