package view

import (

	"encoding/json"
	"github.com/gorilla/mux"

	"io/ioutil"
	"key-value-service/model"
	"key-value-service/service"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

// Metrics per each request for creation of Key-Value pair will be collected (in terms of latency)
// if and only if the request completed successfully.
type Metrics interface {
	// For now let's stick just to latency metric
	MeasureLatency(start time.Time)
}

type ConfigurationEndpoint struct {
	kvService *service.KeyValueService
	metrics   Metrics
}

func NewConfigurationEndpoint(kvService *service.KeyValueService, metrics Metrics, router *mux.Router) *ConfigurationEndpoint {
	c := &ConfigurationEndpoint{kvService: kvService, metrics: metrics}
	router.HandleFunc("/kv-store", c.createKeyValuePairHandler).Methods("POST")
	return c
}

func (c *ConfigurationEndpoint) createKeyValuePairHandler(w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()
	var kv model.KeyValuePair
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error("error while reading data from body:", err)
		http.Error(w, err.Error(), 400)
		return
	}
	err = json.Unmarshal(reqBody, &kv)
	if err != nil {
		log.Error("unable to parse provided body to KeyValuePair:", err)
		http.Error(w, err.Error(), 400)
		return
	}

	createdKV, err := c.kvService.CreateKeyValuePair(&kv)
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Error("unable to create KeyValuePair:", err)
		return
	}

	response, err := json.Marshal(createdKV)
	if err != nil {
		log.Error("unable to serialize KeyValuePair:", err)
		http.Error(w, err.Error(), 500)
		return
	}
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(response)
	if err != nil {
		log.Error("unable to write created KeyValuePair to the response:", err)
		http.Error(w, err.Error(), 500)
		return
	}

	// Metrics are applicable only to requests that succeeded
	c.metrics.MeasureLatency(startTime)
}


